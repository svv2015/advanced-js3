const user1 = {
    name: "John",
    years: 30
  };
  

  const { name: name, years: years, isAdmin = false } = user1;
  

  console.log(name);
  console.log(years);
  console.log(isAdmin);