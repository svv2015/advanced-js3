const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const combiClientsSet = new Set([...clients1, ...clients2]);

const combiClients = [...combiClientsSet];

console.log(combiClients);